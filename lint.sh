#!/usr/bin/env bash
docker run -it -v $PWD:/playbook --workdir=/playbook sdesbure/ansible-lint ansible-lint init.yml
