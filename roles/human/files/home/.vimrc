" Vundle setup begin
set nocompatible              " be iMproved, required

let g:syntastic_sh_checkers = ['shellcheck', 'sh']	" Make shellchecker the default shell checker.
let g:syntastic_check_on_open = 1 " Run syntastic errorchecking on startup
set nu " Line numbers
set clipboard=unnamedplus " Use the OS clipboard as default buffer
colo pablo
set tabstop=4
set shiftwidth=4
set expandtab
set laststatus=2
set splitbelow
set dir=~/tmp
" Custom binding - F3 to clear last-search to remove highlighting
nnoremap <F3> :let @/ = ""<CR>

" Powerline stuff
set guifont=Ubuntu\ Mono\ 12
let g:airline_power_fonts = 1

" Turn on syntax highlighting by default - will cause slow load on massive
" files
syntax on

set backspace=2 " make backspace work like most other apps

nnoremap <F2> :NERDTreeToggle <CR>
set t_Co=256
