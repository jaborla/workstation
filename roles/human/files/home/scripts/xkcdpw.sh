#!/usr/bin/env bash
# Make XKCD-style password
# Licence: WTFPL
totes=10
if [ "$1" != "" ]; then
	totes=$1
fi
for i in `seq 1 $totes`;
do
	#shuf -n4 /usr/share/dict/words | tr '\n' ' ' | tr "'" '_'
	#shuf -n4 /usr/share/dict/words | sed -e ':a;N;$!ba;s/\n/ /g;s/'\''//g;s/\b\(.\)/\u\1/g;s/ //g'
	gshuf /usr/share/dict/words  |grep "^[^']\{3,8\}$" |head -n4 | tr '\n' ' '
	echo
done
