#!/usr/bin/env bash
# Dock-in
# License: WTFPL
#
# Connects to a docker instance that matches the glob provided as first parameter
#
# E.g. "dockin.sh logstash" will connect to a container whose description includes "logstash"
glob="$1"
docker exec -ti $(docker ps | grep "${glob}" | awk {'print $1'}) /bin/bash
