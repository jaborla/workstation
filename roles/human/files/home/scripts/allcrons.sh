#!/usr/bin/env bash
# List crontabs for all users
# LICENCE: WTFPL
for user in $(cut -f1 -d: /etc/passwd)
do 
    echo $user
    crontab -u $user -l
done
