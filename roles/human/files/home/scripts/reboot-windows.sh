#!/usr/bin/env bash
sudo mount /media/juan/WinBoot
cd /media/juan/WinBoot/Boot || exit
cp BCD BCD.$(date +%Y%m%d%H%M%S) || exit
cp BCD.Windows BCD || exit
sudo reboot
