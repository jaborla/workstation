#!/usr/bin/env bash

mkdir ~/tmp
cd ~/tmp || exit

rm -f libffmpeg.so

wget https://github.com/iteufel/nwjs-ffmpeg-prebuilt/releases/download/0.36.4/0.36.4-linux-x64.zip

unzip 0.36.4-linux-x64.zip

cd /usr/lib/x86_64-linux-gnu/opera || exit

sudo cp -f ~/tmp/libffmpeg.so .
