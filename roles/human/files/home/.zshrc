# The following lines were added by compinstall

zstyle ':completion:*' completer _expand _complete _ignored
zstyle ':completion:*' max-errors 1
zstyle :compinstall filename '/home/jborla/.zshrc'

autoload -Uz compinit
compinit
# End of lines added by compinstall
#

# Keep 1000 lines of history within the shell and save it to ~/.zsh_history:
HISTFILE=~/.histfile
HISTSIZE=1000
SAVEHIST=1000

# Keybindings
# (`-e`=emacs | `-v`=vi | no line is also fine)
bindkey -e # Use emacs keybindings even if our $EDITOR is set to vi

## Additional keybindings
# This is to ensure consistent treatment of "special keys" (e.g. Home/End/Delete)
# bindkey "^[[1~" beginning-of-line
# bindkey "^[[2~" quoted-insert
# bindkey "^[[3~" delete-char
# bindkey "^[[4~" end-of-line
# bindkey "^[[5~" beginning-of-history
# bindkey "^[[6~" end-of-history
# bindkey "^H" backward-delete-word
# For rxvt
# bindkey "^[[7~" beginning-of-line
# bindkey "^[[8~" end-of-line
# For non-RedHat/Debian xterm (can't hurt RedHat/Debian xterm)
# bindkey "^[OH" beginning-of-line
# bindkey "^[OF" end-of-line
# For FreeBSD console (including MacOS)
# bindkey "^[[H" beginning-of-line
# bindkey "^[[F" end-of-line
# Completion in the middle of a line (generally equivalent to Tab)
# bindkey '^i' expand-or-complete-prefix
#
#
# aliases unique to zsh
if [ -f "${HOME}/.zsh_aliases" ]; then
   source "${HOME}/.zsh_aliases"
fi
#
# Functions
#
# Some people use a different file for functions
if [ -f "${HOME}/.zsh_functions" ]; then
   source "${HOME}/.zsh_functions"
fi
#
# Oh My ZSH
export ZSH=$HOME/.oh-my-zsh
#ZSH_THEME="philips"
#ZSH_THEME="gallois"
#ZSH_THEME="nanotech"
#ZSH_THEME="sporty_256"
#ZSH_THEME="blinks"
#ZSH_THEME="crunch"
ZSH_THEME="agnoster"
#
# EPS1=%F{cyan}[%F{white}%T%F{cyan}]
RPROMPT='%F{red}(%F{white}%T%F{red})'
zstyle :omz:plugins:ssh-agent identities jborla_rsa
CASE_SENSITIVE="true"
DISABLE_AUTO_UPDATE="true"
DEFAULT_USER="jborla"
DEFAULT_HOST="CON-GKCNP72"
export GIT_DISCOVERY_ACROSS_FILESYSTEM=1
plugins=(git git-extras ssh-agent zsh-syntax-highlighting docker)
source $ZSH/oh-my-zsh.sh
LS_COLORS=$LS_COLORS'tw=7;36:ow=7;35:' ; export LS_COLORS
zstyle ':completion:*' list-colors "${(@s.:.)LS_COLORS}"
#
# Include bash aliases
if [ -f "${HOME}/.aliases" ]; then
   source "${HOME}/.aliases"
fi

# Shortcut hashes
hash -d -- p=~/Documents/projects
hash -d -- w=~/Documents/workspace

#
#
# Custom Prompt
# if [ -f "${HOME}/.zsh_prompt" ]; then
#   source "${HOME}/.zsh_prompt"
# fi
