# TODO
* Add automatic ffmpeg to Opera
  * [https://forums.opera.com/topic/27474/please-explain-to-this-rookie-how-to-install-codecs-to-make-videos-work/13]

* Finish roles
  * speedrunning
    * Add LiveSplit

* Add tags
  * Roles to replace init-custom.yml
  * Repo management
  * Package installation (state: latest?)
  * Configuration

* Make function inside docker container
  * Is "Ansible and prereqs" really a lighter requirement than "Docker"?
    * Not really relevant.  Should be compatible with both

* Automatically create my primary user
* Automatically generate SSH keys for the new user and put them in place

* Configure
  * sshd
  * git
    * user info
    * default editor
    * aliases
  * default editor (nano?  Freaking what?)
  * terminal colors
  * browser logins and extensions
  * sublime plugins and themes
