#!/usr/bin/env bash
if [ "$1" != '' ]; then
	ansible-playbook -i inventories/home init-custom.yml --ask-vault-pass --ask-become-pass -vv --extra-vars "custom_role='$1'"
else
	ansible-playbook -i inventories/home "init.yml" --ask-become-pass --ask-vault-pass -vvv
fi
